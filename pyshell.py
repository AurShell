#!/usr/bin/python
# -*- coding: utf-8-*-

"""
Aur Shell is simple shell framework. 

To use it, write some plugins that Aur Shell would use.
"""

import cmd
import string
import sys
import os
#import os.path
#import types
import readline 

import conf
from showinfo import Put

__version__ = "0.1"


class AurShell(cmd.Cmd):
    """Interactive shell framework.
    To use it, write some modules, becouse by default it's very poor shell.
    """
    def __init__(self):
        cmd.Cmd.__init__(self)
        # use it instead of plain `print`
        self.put = Put()
        self.conf = conf
        # prefix for method callable by shell
        self.cmdprefix = "do_" 
        self.prompt = conf.shell_prompt
        # plugins dict
        self.commands = self.load_plugins(conf.modules_path)
        # intro message
        self.intro = "\n\tWelcome to aurShell v%(__version__)s\n" % globals()
        # load history, create empty file if history doesn't exist
        if not os.path.isfile(conf.history_file):
            open(conf.history_file, "w").close()
        readline.read_history_file(conf.history_file)
        readline.set_history_length(conf.history_length)
        # create build_dir if doesn't exist
        if not os.path.isdir(conf.build_dir):
            os.mkdir(conf.build_dir)

    def load_plugins(self, modules_path):
        """Load all commands modules from modules_path direcotory."""
        command_modules = []
        # adding plugins path to sys.path
        if modules_path not in sys.path: 
            sys.path.append(modules_path)    
        # importing all modules from modules_path
        for module_name in os.listdir(modules_path):
            module_path = os.path.abspath(
                    string.join(modules_path, module_name))
            (module_name, module_extension) = os.path.splitext(module_name)
            # if it shouldn't be load
            if os.path.islink(module_path) or  \
                    module_path == __file__ or \
                    module_extension != ".py":
                        continue
            for key in sys.modules.keys():
                if key == module_name: 
                    del sys.modules[key]
            module = __import__(module_name)
            command_modules.append({"obj": module_name, "name": module,})
        # search for class in each module
        module_objs = {}
        for module in command_modules:
            for obj in dir(module["name"]):
                # TODO 
                # better class serch 2008-01-27 16:56:42
                if not obj.startswith("_"):
                    try:
                        # check if it's method
                        x = getattr(module['name'], obj)(self.put, conf)
                        # create instance for each class
                        # as agrument give it Put() class instance
                        module_objs[obj] = \
                            getattr(module['name'], obj)(self.put, conf)
                    except TypeError:
                        pass
        return module_objs
    
    def precmd(self, cmd):
        # check if alias exists and if so, replace command
        for alias in conf.alias.keys():
            # command should start with alias, but:
            # - next char should be white space, or
            # - there's no next char
            if cmd.startswith(alias) and \
                    (len(cmd) <= len(alias) or cmd[len(alias)] == " "):
                cmd = cmd.replace(alias, conf.alias[alias], 1)
        return cmd

    def do_shell(self, cmd):
        """System shell command, for commads which starts with !"""
        os.system(cmd)

    def default(self, cmd=None):
        """Run commands.

        cmd = (<class>, [method], [arg1], [arg2], ...)
        """
        # cmd[0] should be class name
        # cmd[1] should be method name (or arugmet if class is callable)
        # cmd[1] can be empty
        # cmd[2:] should be method argumments, can be empty
        cmd = cmd.split()
        # operations for single command with no arguments
        if len(cmd) == 1:
            # if there's no such command (or plugin class)
            if not cmd[0] in self.commands.keys():
                self.put("%s : command not found." % cmd[0])
            else:
                self.put("%s : bad usege. Try to run help." % cmd[0])
        # if command was called with arguments
        elif len(cmd) > 1:
            cmd[1] = self.cmdprefix + cmd[1]
            if not cmd[0] in self.commands.keys():
                self.put("%s : command not found." % cmd[0])
            # if method named arg[1] exist in class arg[0], try to run it
            elif cmd[1] in dir(self.commands[cmd[0]]):
                #print dir(self.commands[cmd[0]])
                try:
                    getattr(self.commands[cmd[0]], cmd[1])(*cmd[2:])
                except TypeError:
                    # show __doc__ if exist
                    doc = getattr(self.commands[cmd[0]], cmd[1])
                    if doc.__doc__:
                        self.put(doc.__doc__)
                    else:
                        self.put("%s : bad usage" % cmd[1][3:])
            else:
                # try tu run __call__() method
                try:
                    cmd[1] = cmd[1][len(self.cmdprefix):]
                    getattr(self.commands[cmd[0]], "__call__")(*cmd)
                except TypeError:
                    self.put("%s : bad usage" % cmd[0])

    def completenames(self, text, *ignored):
        """Complete commands"""
        dotext = self.cmdprefix + text
        # local methods
        local_cmd_list = \
                [a[3:] + " " for a in self.get_names() if a.startswith(dotext)]
        # + all metrods from modules
        module_cmd_list = \
                [a + " " for a in self.commands.keys() if a.startswith(text)]
        # + all aliases
        aliases_cmd_list = \
                [a + " " for a in self.conf.alias.keys() if a.startswith(text)]
        return local_cmd_list + module_cmd_list + aliases_cmd_list

    def completedefault(self, text, line, begidx, endidx):
        """Complete commands argument"""
        dotext = self.cmdprefix + text
        line = line.split()
        # if only commands was given
        if len(line) == 1:
            cmds = [a[3:] + " " for a in dir(self.commands[line[0]]) \
                    if a.startswith(dotext)]
        elif len(line) == 2:
            cmds = [a[3:] + " " for a in dir(self.commands[line[0]]) \
                    if a.startswith(dotext)]
        # else don't complete (or should I?)
        else:
            cmds = []
        return cmds

    def do_help(self, arg):
        """Show help for commands"""
        arg = arg.split()
        if not arg:
            self.put("Usage: help <command>")
        elif len(arg) == 1:
            # first - build-in methods
            if self.cmdprefix + arg[0] in dir(self):
                doc = getattr(self, self.cmdprefix + arg[0]).__doc__
                if doc:
                    self.put(doc)
            # modules help
            else:
                try:    
                    # try to run help() method
                    self.put(getattr(self.commands[arg[0]], "help")())
                except AttributeError:
                    # try to show __doc__
                    if self.commands[arg[0]].__doc__:
                        self.put(self.commands[arg[0]].__doc__)
                    else:
                        self.put("No help found.")
                except KeyError: 
                    self.put("No help found.")

        elif len(arg) == 2:
            try:
                if arg[0] in self.commands.keys():
                    arg[1] = self.cmdprefix + arg[1]
                    doc = getattr(self.commands[arg[0]], arg[1]).__doc__
                    self.put(doc)
            except AttributeError:
                self.put("%s : no help found" % arg[1][3:])
        else:
            self.put("Try to do something else.\nThis option doesn't work well now.")

    def do_clear(self, *ignored):
        """Clear the screen"""
        # TODO 2008-02-09 20:44:50
        self.put("Todo, sorry")

    def do_history(self, hnumb=None, *ignored):
        """Show the history"""
        # TODO better history listing 
        # 2008-01-27 18:51:22
        # print whole history
        if not hnumb:
            for number in range(1, conf.history_length):
                cmd = readline.get_history_item(number)
                if not cmd:
                    break
                self.put("%6d  %s" % (number, cmd))
        # for history range 12-22 or -22 or 22- 
        else:
            try:
                if "-" in hnumb:
                    if hnumb[-1] == "-" or hnumb[0] == "-":
                        start = int(hnumb.replace("-", " "))
                        end = conf.history_length
                    else:
                        start, end = hnumb.split("-")
                        start = int(start)
                        end = int(end) + 1
                    for number in range(start, end):
                        cmd = readline.get_history_item(number)
                        if not cmd:
                            break
                        self.put("%6d  %s" % (number, cmd))
                else:
                    hnumb = int(hnumb)
                    self.put(readline.get_history_item(hnumb))
            except ValueError:
                self.put("""Bad value.
Usage: history <number or range>
    history 11-20  -> from 11 to 20
    history 22-     -> from 22 fo the end of history file
    history -22     -> same as 22-""")

    def do_quit(self, *ignored):
        """Quit from shell"""
        if conf.history_length:
            readline.write_history_file(conf.history_file)
        print ""
        sys.exit(1)
    
    # function aliases
    do_EOF = do_quit
    do_exit = do_quit
 

if __name__ == "__main__":
    shell = AurShell()
    try:
        shell.cmdloop()
    except KeyboardInterrupt:
        # TODO 2008-01-27 16:56:31
        sys.exit(shell.do_quit())
