import string
import os
import os.path
import sys
import re
import shutil
import makepkg


class abs(makepkg._make_package):
    """
    Do some stuff with ABS. Search, compile, install. 
    You know, everything that you need to be happy
    """
    def __init__(self, put, conf):
        self.put = put
        self.conf = conf

    def do_builddir(self, bdir=None, *ignore):
        # shouldn't this be plugin independent method?
        if bdir:
            if not os.path.isdir(bdir):
                if self.put.ask("No such directory, create it?"):
                    try:
                        os.mkdir(bdir)
                        self.conf.build_dir = bdir
                    except OSError:
                        self.put("Permission denied.", "RED", True)
            else:
                self.conf.build_dir = bdir
        self.put("Build directory: ", "GREEN", False)
        self.put(self.conf.build_dir)
    
    def do_info(self, pkgname, *ignore):
        """Show PKGBUILD info"""
        for root, dirs, files in os.walk(self.conf.abs_path):
            for d in dirs:
                if pkgname == d:
                    self.show_pkgbuild(os.path.join(root, d, "PKGBUILD"))
                    return 

    def do_search(self, pkgname, *ignore):
        """Find PKGBUILD in ABS file tree."""
        # TODO 2008-02-13 20:08:29
        # Regular expression operations for searching
        pkgbuild_found = []
        for root, dirs, files in os.walk(self.conf.abs_path):
            for d in dirs:
                if pkgname in d:
                    try:
                        name = str(d)
                        for line in open(os.path.join(root, d, "PKGBUILD"), "r"):
                            if line.startswith("pkgver"):
                                name += " - " + line.split("=")[1][:-1]
                        self.put("[abs] ", "YELLOW", False)
                        self.put(name.ljust(32), "WHITE", False)
                        self.put(root)
                    except IOError:
                        pass

    def do_compile(self, pkgname, *ignore):
        """Run conf.compile_cmd in given path"""
        for root, dirs, files in os.walk(self.conf.abs_path):
            for d in dirs:
                if d == pkgname:
                    path = os.path.join(root, d)
                    build_path = os.path.join(self.conf.build_dir, d)
                    if os.path.isdir(build_path):
                        if self.put.ask("Directory exist. Delete it?"):
                            shutil.rmtree(build_path)
                        else:
                            self.put("Nothing to do.")
                            return
                    shutil.copytree(path, build_path)
                    os.system("cd %s && %s " % (build_path, self.conf.compile_cmd))
                    return
        self.put("PKGBUILD not found.")

    def do_install(self, pkgname, *ignore):
        """Install package from conf.build_path + pkgname."""
        build_path = os.path.join(self.conf.build_dir, pkgname)
        if not os.path.isdir(build_path):
            self.put("No package found. Try to compile it first.")
            return
        self.run_install(build_path)
