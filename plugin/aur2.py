#!/usr/bin/python
# -*- coding: utf-8-*-

import os


class aur(object):
    """
    Search and build packages with PKGBUILDs from AUR.
    As for now, it's not ready to use. AUR2 need to be
    done first.
    """
    def __init__(self, put, conf):
        self.conf = conf
        self.put = put

    def do_seturl(self, url=None, *ignore):
        """Set non-default url for AUR database"""
        if not url:
            self.put("AUR url: ", "GREEN", False)
            self.put(self.conf.aur_url)
        else:
            self.conf.aur_url = url
            self.put("New AUR url: ", "GREEN", False)
            self.put(self.conf.aur_url)

    def do_search(self, *args):
        """Search package with <arg list> in it's name or description"""
        #TODO 2008-02-11 15:35:33
        self.put("TODO", "RED", True)
        self.put("AUR2 search:", color="GREEN")
        self.put(" ".join(args))

            
    def do_upgrade(self, *ignore):
        """Upgrade packages from AUR"""
        #TODO 2008-02-11 15:35:28
        self.put("TODO", "RED", True)
        for pkg in os.popen("pacman -Qm"):
            pkg, ver = pkg.split()
            self.put(" }-->", "GREEN", False)
            self.put(pkg.ljust(32), "green", False)
            self.put(ver, newline=True)
            

