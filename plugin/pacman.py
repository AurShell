import os



class pacman(object):
    """Use it like it was pacman.
    It's just pacman parser, nothing more.
    """
    def __init__(self, put, conf):
        self.put = put
        self.conf = conf
        self.pacman_cmd = "sudo pacman "

    def __call__(self, *args):
        os.system( self.pacman_cmd + " ".join(args))


