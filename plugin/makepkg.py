import string
import os
import os.path
import sys
import re
import shutil


class _make_package(object):
    """
    bla bla bla
    """
    def __init__(self, put, conf):
        self.put = put
        self.conf = conf

    def do_builddir(self, bdir=None, *ignore):
        if bdir:
            if not os.path.isdir(bdir):
                if self.put.ask("No such directory, create it?"):
                    try:
                        os.mkdir(bdir)
                        self.conf.build_dir = bdir
                    except OSError:
                        self.put("Permission denied.", "RED", True)
            else:
                self.conf.build_dir = bdir
        self.put("Build directory: ", "GREEN", False)
        self.put(self.conf.build_dir)
    
    def show_pkgbuild(self, path, *ignore):
        """Show PKGBUILD info"""
        # TODO 2008-02-14 11:16:10
        # color output?
        for line in open(path, "r"):
            self.put(line, newline=False)
    
    def copy_dir(self, pfrom, pto):
        """
        Copy files from pfrom to pto.
        If directory pto exists, ask if should delete it first.
        """
        if os.path.isdir(pto):
            if self.put.ask("Directory exist. Delete it?"):
                shutil.rmtree(pto)
            else:
                self.put("Nothing to do.")
                return False
        shutil.copytree(pfrom, pto)
        return True

    def run_compile(self, path):
        """
        Try to compile with PKGBUILD given in path.
        Use self.conf.compile_cmd to make package.
        """
        if not "PKGBUILD" in os.path.listrid(compile_path):
            self.put("PKGBUILD not found.")
            return False
        os.system("cd %s && %s " % (path, self.conf.compile_cmd))

    def run_install(self, path, *ignore):
        """
        Search for packages in given path, choose one and install it
        with self.conf.install_cmd
        """
        pkglist = []
        for pkgfile in os.listdir(path):
            if pkgfile.endswith("pkg.tar.gz"):
                pkglist.append(pkgfile)
        if len(pkglist) == 0:
            self.put("No package found. Try to compile it first.")
        elif len(pkglist) == 1:
                os.system("cd %s && %s %s" % \
                        (path, self.conf.install_cmd, pkglist[0]))
        else:
            self.put("More than one package found, choose one (empty to cancel):")
            for number, pkg in enumerate(pkglist):
                self.put("[ ", "BLUE")
                self.put(str(number), "BOLD")
                self.put(" ] ", "BLUE")
                self.put(pkg)
            try:
                numb = self.put.read()
                # if empty, cancel installation
                if not numb: 
                    return True
                else:
                    numb = int(numb)
            except ValueError:
                self.put("Bad value.", "RED", True)
                self.run_install(path)
                return False

            try:
                os.system("cd %s && %s %s" % \
                        (path, self.conf.install_cmd, pkglist[numb]))
            except IndexError:
                self.put("Bad value. BLAH!", "RED", True)
                self.run_install(path)

