#!/usr/bin/python
# -*- coding: utf-8-*-

"""
Configuration file.
"""

from colors import *
import os
import os.path

alias = {
        "h"         : "history",
        "info"      : "help",
        "up"        : "aur upgrade",
        "!p"        : "!pacman -U *.pkg.tar.gz",
        "!sx"        : "!pacman -Ss xorg | grep  ",
        }


modules_path = os.path.join(os.getcwd(), "plugin/")
# history file, by default in home dir
history_file = os.path.join(os.path.expanduser("~"), ".aurshell_history")
history_length = 500
# source compilation path, by default in home dir
build_dir = os.path.join(os.path.expanduser("~"), "aurshell")
# how to install package?
install_cmd = "sudo pacman -U "
# compile command
compile_cmd = "makepkg -f "
# AUR url
aur_url = "http://aur.archlinux.com"
# ABS path
abs_path = "/var/abs"

# shell prompt
shell_prompt = color["BLUE"] + " [ " + color["WHITE"] + "aurshell" + \
               color["BLUE"] + " ] " + color["none"]


