import sys

try:
    import conf
except ImportError:
    sys.exit("Can't find conf module.")
    


class Put(object):
    """Show info class"""
    def __init__(self, stdout=None, stdin=None):
        """Set stdout and stdin"""
        if not stdout:
            self.stdout = sys.stdout
        else:
            self.stdout = stdout
        if not stdin:
            self.stdin = sys.stdin
        else:
            self.stdin = stdin

    def __call__(self, message, color=None, newline=True, *ignore):
        """Show message on stdout

        message - message that would be shown
        color - opional color of the message
        newline - end with newline?
        """
        if not color:
            self.show(message, newline)
        else:
            self.color(message, color, newline)


    def show(self, message="",  newline=True, *ignore):
        """Default message printer"""
        if newline:
            self.stdout.write(message + "\n") 
        else:
            self.stdout.write(message) 
             
    def read(self, message=""):
        if message:
            self.show(message + " ", newline=False)
        return self.stdin.readline()

    def ask(self, question, color=None, answer=" [Y/n]"):
        while True:
            self.color(question + answer, color, False)
            a = self.stdin.readline()
            if a in "Yy":
                return True
            elif a in "Nn":
                return False


    def color(self, msg, color=None, newline=False):
        """Print message with colors
        By default it doesn't end with new line character.
        """
        if not color:
            color = "none"
        self.show(conf.color[color] + msg + conf.color["none"], newline)
        
        
